import ChatModel from "../database/models/ChatModel";
import UserChatModel from "../database/models/UserChatModel";
import ApiError from "../exceptions/apiError";
import TokenService from "./tokenService";
import UserModel from "../database/models/UserModel";

export default class ChatService {
  static async createChat(chatName: string, refreshToken: string) {
    const userData = await TokenService.validateRefreshToken(refreshToken);
    const tokenFromDatabase = await TokenService.findToken(refreshToken);
    if (!userData || !tokenFromDatabase) {
      throw ApiError.UnathorizedError();
    }
    const user = await UserModel.findOne({
      where: { id: tokenFromDatabase.id },
    });
    if (!user) {
      throw ApiError.UnathorizedError();
    }
    const chat = await ChatModel.create({ chatName });
    await UserChatModel.create({ userId: user.id, chatId: chat.id });
    return chat;
  }

  static async addUserToChat(userId: number, chatId: number) {
    const userChat = await UserChatModel.findOne({ where: { userId, chatId } });
    if (userChat) {
      throw ApiError.BadRequest("Пользователь уже в чате");
    }
    const chat = UserChatModel.create({ userId, chatId });
    return chat;
  }

  static async removeUserFromChat(userId: number, chatId: number) {
    const userChat = await UserChatModel.findOne({ where: { userId, chatId } });
    if (!userChat) {
      throw ApiError.BadRequest("Пользователь не в чате");
    }
    await userChat.destroy();
    return userChat;
  }

  static async getChatsByUserId(refreshToken: string) {
    const userData = await TokenService.validateRefreshToken(refreshToken);
    const tokenFromDatabase = await TokenService.findToken(refreshToken);
    if (!userData || !tokenFromDatabase) {
      throw ApiError.UnathorizedError();
    }
    const user = await UserModel.findOne({
      where: { id: tokenFromDatabase.id },
    });
    if (!user) {
      throw ApiError.UnathorizedError();
    }
    const userChatsId = await UserChatModel.findAll({
      where: { userId: user.id },
    });
    if (!userChatsId) {
      throw ApiError.BadRequest("У пользователя нет чатов");
    }
    const chats = await ChatModel.findAll({
      where: { id: userChatsId.map((userChat) => userChat.chatId) },
    });
    return chats;
  }

  static async getChatById(chatId: number) {
    const chat = await ChatModel.findOne({ where: { id: chatId } });
    if (!chat) {
      throw ApiError.BadRequest("Чат не найден");
    }
    return chat;
  }

  static async getMessagesByChatId(chatId: number) {
    const messages = await ChatModel.findOne({ where: { id: chatId } });
    if (!messages) {
      throw ApiError.BadRequest("В чате нет сообщений");
    }
    return messages;
  }
}
