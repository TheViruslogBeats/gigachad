import UserModel from "../database/models/UserModel";
import ApiError from "../exceptions/apiError";
import bcrypt from "bcrypt";
import UserDto from "../dtos/userDto";
import TokenService from "./tokenService";

export default class AuthService {
  static async register(username: string, password: string) {
    const candidate = await UserModel.findOne({ where: { username } });
    if (candidate) {
      throw ApiError.BadRequest("Пользователь с таким именем уже существует");
    }
    const hashedPassword = await bcrypt.hash(password, 6);
    const user = await UserModel.create({ username, hashedPassword });
    const userDto = new UserDto(user);
    const tokens = await TokenService.generateTokens({ ...userDto });
    await TokenService.saveToken(userDto.id, tokens.refreshToken);
    return { ...tokens, user: userDto };
  }

  static async login(username: string, password: string) {
    const user = await UserModel.findOne({ where: { username } });
    if (!user) {
      throw ApiError.BadRequest("Пользователь с таким именем не найден");
    }
    const isPassEquals = await bcrypt.compare(password, user.hashedPassword);
    if (!isPassEquals) {
      throw ApiError.BadRequest("Неверный пароль");
    }
    const userDto = new UserDto(user);
    const tokens = await TokenService.generateTokens({ ...userDto });
    await TokenService.saveToken(userDto.id, tokens.refreshToken);
    return { ...tokens, user: userDto };
  }

  static async logout(refreshToken: string) {
    return await TokenService.removeToken(refreshToken);
  }

  static async refresh(refreshToken: string) {
    if (!refreshToken) {
      throw ApiError.UnathorizedError();
    }
    const userData = await TokenService.validateRefreshToken(refreshToken);
    const tokenFromDatabase = await TokenService.findToken(refreshToken);
    if (!userData || !tokenFromDatabase) {
      throw ApiError.UnathorizedError();
    }
    const user = await UserModel.findOne({
      where: { id: tokenFromDatabase.id },
    });
    if (!user) {
      throw ApiError.UnathorizedError();
    }
    const userDto = new UserDto(user);
    const tokens = await TokenService.generateTokens({ ...userDto });
    await TokenService.saveToken(userDto.id, tokens.refreshToken);
    return { ...tokens, user: userDto };
  }
}
