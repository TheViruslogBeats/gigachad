import UserModel from "../database/models/UserModel";
import UserDto from "../dtos/userDto";
import ChatService from "./chatService";
import ApiError from "../exceptions/apiError";

export default class UserService {
  static async getAllUsers() {
    const users = await UserModel.findAll();
    const usersDtos = users.map((user) => {
      return new UserDto(user);
    });
    return usersDtos;
  }

  static async getUserById(id: number) {
    const user = await UserModel.findOne({ where: { id } });
    if (!user) {
      throw ApiError.BadRequest("Пользователь с таким id не найден");
    }
    const userDto = new UserDto(user);
    return userDto;
  }
}
