import UserDto from "../dtos/userDto";
import dotenv from "dotenv";
import jwt from "jsonwebtoken";
import TokenModel from "../database/models/TokenModel";

dotenv.config();

export default class TokenService {
  static async generateTokens(payload: UserDto) {
    const awaiter = Promise.all([
      jwt.sign(payload, process.env.JWT_ACCESS_SECRET!, { expiresIn: "30s" }),
      jwt.sign(payload, process.env.JWT_REFRESH_SECRET!, { expiresIn: "30d" }),
    ]);
    let tokens: {
      accessToken: string;
      refreshToken: string;
    } = { accessToken: "", refreshToken: "" };
    await awaiter.then((value) => {
      tokens = {
        accessToken: value[0],
        refreshToken: value[1],
      };
    });
    return tokens;
  }

  static async validateAccessToken(token: string) {
    try {
      const decoded = jwt.verify(token, process.env.JWT_ACCESS_SECRET!);
      return decoded;
    } catch (e) {
      return null;
    }
  }

  static async validateRefreshToken(token: string) {
    try {
      return jwt.verify(token, process.env.JWT_REFRESH_SECRET!);
    } catch (e) {
      return null;
    }
  }

  static async saveToken(userId: number, refreshToken: string) {
    try {
      const tokenData = await TokenModel.findOne({ where: { userId } });
      if (tokenData) {
        tokenData.refreshToken = refreshToken;
        return await tokenData.save();
      }
      return await TokenModel.create({ userId, refreshToken });
    } catch (e) {
      return null;
    }
  }

  static async removeToken(refreshToken: string) {
    try {
      return await TokenModel.destroy({ where: { refreshToken } });
    } catch (e) {
      return null;
    }
  }

  static async findToken(refreshToken: string) {
    try {
      return await TokenModel.findOne({ where: { refreshToken } });
    } catch (e) {
      return null;
    }
  }
}
