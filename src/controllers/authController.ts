import AuthService from "../services/authService";

import {Request, Response, NextFunction} from "express";
import ApiError from "../exceptions/apiError";

export default class AuthController {
  static async login(req: Request, res: Response, next: NextFunction) {
    try {
      const {username, password} = req.body
      if(!username || !password) {
        return res.status(400).json({message: "Пожалуйста, введите логин и пароль"})
      }
      const user = await AuthService.login(username, password)
      res.cookie("refreshToken", user.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000})
      return res.json(user)
    } catch (e) {
      next(e)
    }
  }

  static async logout(req: Request, res: Response, next: NextFunction) {
    try {
      const {refreshToken} = req.cookies
      if(!refreshToken) {
        throw ApiError.UnathorizedError()
      }
      const token = await AuthService.logout(refreshToken)
      res.clearCookie("refreshToken")
      return res.json({message: "Вы вышли из аккаунта"})
    } catch (e) {
      next(e)
    }
  }

  static async refresh(req: Request, res: Response, next: NextFunction) {
    try {
      const {refreshToken} = req.cookies
      const user = await AuthService.refresh(refreshToken)
      res.cookie("refreshToken", user.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000})
      return res.json(user)
    } catch (e) {
      next(e)
    }
  }

  static async register(req: Request, res: Response, next: NextFunction) {
    try {
      const {username, password} = req.body
      if(!username || !password) {
        return res.status(400).json({message: "Пожалуйста, введите логин и пароль"})
      }
      const user = await AuthService.register(username, password)
      res.cookie("refreshToken", user.refreshToken, {maxAge: 30 * 24 * 60 * 60 * 1000})
      return res.json(user)
    } catch (e) {
      next(e)
    }
  }
}