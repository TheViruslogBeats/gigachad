import ChatService from "../services/chatService";
import { NextFunction, Request, Response } from "express";

export default class ChatController {
  static async getChats(req: Request, res: Response, next: NextFunction) {
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken) {
      return res.status(401).json({ message: "Unauthorized" });
    }
    const chats = await ChatService.getChatsByUserId(refreshToken);
    return res.json(chats);
  }

  static async createChat(req: Request, res: Response, next: NextFunction) {
    const refreshToken = req.cookies.refreshToken;
    const { chatName } = req.body;
    const chat = await ChatService.createChat(chatName, refreshToken);
    return res.json(chat);
  }

  static async addUserToChat(req: Request, res: Response, next: NextFunction) {
    const refreshToken = req.cookies.refreshToken;
    const { userId, chatId } = req.body;
    const userChat = await ChatService.addUserToChat(userId, chatId);
    return res.json(userChat);
  }
}
