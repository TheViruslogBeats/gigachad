import express from "express";
import ws from "ws";
import sequelize from "./database";
import UserModel from "./database/models/UserModel";
import ChatModel from "./database/models/ChatModel";
import UserChatModel from "./database/models/UserChatModel";
import MessageModel from "./database/models/MessageModel";
import TokenModel from "./database/models/TokenModel";
import errorMiddleware from "./middlewares/errorMiddleware";
import v1MainRouter from "./routes/v1MainRouter";

import cookieParser from "cookie-parser";
import cors from "cors";
import compression from "compression";
//TO ENV
const MAIN_PORT = 5000;
const WS_PORT = 5001;
const pid = process.pid;

export const wsServer = new ws.Server({ port: WS_PORT, path: "/chat" }, () => {
  console.log(`WebSocket Server started on PORT = ${WS_PORT}, PID: ${pid}`);
});

const whiteList = [process.env.CLIENT_URL, process.env.ADMIN_URL, undefined];

const app = express();
app.use(compression());
app.use(cookieParser());
app.use(
  cors({
    credentials: true,
    origin: function (origin, callback) {
      if (whiteList.indexOf(origin) !== -1) {
        callback(null, true);
      }
    },
  }),
);
app.use(express.json());
app.use("/v1", v1MainRouter);

app.use(errorMiddleware);

const startServer = () => {
  try {
    wsServer.on("connection", (ws) => {
      try {
        ws.on("message", (m) => {
          console.log(JSON.parse(m.toString()));
        });
        //@ts-ignore
        ws.on("error", (e) => ws.send(e));
      } catch (e) {
        console.log(e);
      }
    });
    sequelize.authenticate().then(() => {
      sequelize.sync({ alter: true }).then(() => {
        console.log(
          "Connection with Data Base has been established successfully.",
        );
        UserModel.sync({ alter: true });
        ChatModel.sync({ alter: true });
        UserChatModel.sync({ alter: true });
        MessageModel.sync({ alter: true });
        TokenModel.sync({ alter: true });
      });
      app.listen(MAIN_PORT, () => {
        console.log(`Server started on PORT = ${MAIN_PORT}, PID: ${pid}`);
      });
    });
  } catch (e) {
    console.log(e);
  }
};

startServer();
