import {DataTypes, Model, Optional} from "sequelize";
import sequelize from "../index";

export interface ChatModelI {
  id: number,
  chatName: string,
}

type ChatModelCreation = Optional<ChatModelI, "id">

class ChatModel extends Model<ChatModelI, ChatModelCreation>{
  declare id: number;
  declare chatName: string;
}

ChatModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  chatName: {
    type: DataTypes.STRING,
    allowNull: false,
  }
}, {sequelize, tableName: "chat"})

export default ChatModel