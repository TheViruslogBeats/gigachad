import {DataTypes, Model, Optional} from "sequelize";
import sequelize from "../index";
import ChatModel from "./ChatModel";
import UserModel from "./UserModel";

export interface UserChatModelI {
  id: number,
  userId: number,
  chatId: number
}

type UserChatModelCreation = Optional<UserChatModelI, "id">

class UserChatModel extends Model<UserChatModelI, UserChatModelCreation>{
  declare id: number;
  declare userId: number;
  declare chatId: number;
}

UserChatModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  chatId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  }
}, {sequelize, tableName: "user_chat"})

ChatModel.belongsToMany(UserModel, {through: UserChatModel})
UserModel.belongsToMany(ChatModel, {through: UserChatModel})
export default UserChatModel