import {DataTypes, Model, Optional} from "sequelize";
import sequelize from "../index";
import UserModel from "./UserModel";
import ChatModel from "./ChatModel";

export interface MessageModelI {
  id: number
  message: string
  chatId: number
  userId: number
}
type MessageModelCreation = Optional<MessageModelI, "id">
class MessageModel extends Model<MessageModelI, MessageModelCreation>{
  declare id: number;
  declare message: string;
  declare chatId: number;
  declare userId: number;
}

MessageModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  message: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  chatId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  }
}, {sequelize, tableName: "message"})

UserModel.hasMany(MessageModel, {
  sourceKey: 'id',
  foreignKey: 'userId',
})

ChatModel.hasMany(MessageModel, {
  sourceKey: 'id',
  foreignKey: 'chatId',
})

export default MessageModel