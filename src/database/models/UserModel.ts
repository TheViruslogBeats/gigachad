import {DataTypes, Model, Optional} from "sequelize";
import sequelize from "../index";
import TokenModel from "./TokenModel";

export interface UserModelI {
  id: number,
  username: string,
  hashedPassword: string
}

type UserModelCreation = Optional<UserModelI, 'id'>

class UserModel extends Model<UserModelI, UserModelCreation> {
  declare id: number;
  declare username: string;
  declare hashedPassword: string;
}

UserModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  hashedPassword: {
    type: DataTypes.STRING,
    allowNull: false
  }
}, {sequelize, tableName: 'user'});

UserModel.hasOne(TokenModel, {
  sourceKey: 'id',
  foreignKey: 'userId',
})

export default UserModel