import {DataTypes, Model, Optional} from "sequelize";
import sequelize from "../index";

export interface TokenModelI {
  userId: number
  id: number,
  refreshToken: string
}

type TokenModelCreation = Optional<TokenModelI, "id">

class TokenModel extends Model<TokenModelI, TokenModelCreation>{
  declare id: number;
  declare refreshToken: string;
  declare userId: number
}

TokenModel.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  refreshToken: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  }

}, {sequelize, tableName: "token"})

export default TokenModel