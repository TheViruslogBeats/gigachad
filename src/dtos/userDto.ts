import {UserModelI} from "../database/models/UserModel";

export default class UserDto {
  id: number;
  username: string;

  constructor(model: UserModelI) {
    this.id = model.id;
    this.username = model.username;
  }
}