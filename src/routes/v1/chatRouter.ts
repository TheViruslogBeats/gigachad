import { Router } from "express";
import ChatController from "../../controllers/chatController";

const router = Router();

router.get("/chats", ChatController.getChats);

export default router;
