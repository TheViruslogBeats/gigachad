import {Router} from "express";
import authRouter from "./v1/authRouter";

const router = Router();

router.use('/auth', authRouter);

export default router;