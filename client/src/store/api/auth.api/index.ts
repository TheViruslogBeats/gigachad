import { $API } from "../index.ts";

interface IRequestLogin {
  username: string;
  password: string;
}

export interface IResponseLogin {
  accessToken: string;
  refreshToken: string;
  user: User;
}

export interface User {
  id: number;
  username: string;
}

export const authApi = $API.injectEndpoints({
  endpoints: (builder) => ({
    apiAuthPostRegister: builder.mutation<IResponseLogin, IRequestLogin>({
      query: (body) => ({
        url: "auth/register",
        method: "POST",
        body,
      }),
      invalidatesTags: ["Login"],
    }),

    apiAuthPostLogin: builder.mutation<IResponseLogin, IRequestLogin>({
      query: (body) => ({
        url: "auth/login",
        method: "POST",
        body,
      }),
      invalidatesTags: ["Login"],
    }),

    apiAuthLogout: builder.mutation<void, void>({
      query: () => ({
        url: "auth/logout",
      }),
      invalidatesTags: ["Login"],
    }),

    apiAuthRefresh: builder.mutation<IResponseLogin, void>({
      query: () => ({
        url: "auth/refresh",
      }),
      invalidatesTags: ["Login"],
    }),
  }),
});

export const {
  useApiAuthPostRegisterMutation,
  useApiAuthPostLoginMutation,
  useApiAuthLogoutMutation,
  useApiAuthRefreshMutation,
} = authApi.endpoints;
