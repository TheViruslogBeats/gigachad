import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query";
export const $API = createApi({
  reducerPath: "@@Api",
  baseQuery: fetchBaseQuery({
    baseUrl: `${window.location.origin}/api/v1/`,
    credentials: "include",
  }),
  tagTypes: ["VirusBeats", "Login"],
  endpoints: () => ({}),
});
