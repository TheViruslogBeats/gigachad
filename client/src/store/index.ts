import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import { $API } from "./api";

const rootReducer = combineReducers({
  [$API.reducerPath]: $API.reducer,
});

export const setupStore = () => {
  return configureStore({
    reducer: rootReducer,
  });
};

export type RootStateType = ReturnType<typeof rootReducer>;
export type AppStoreType = ReturnType<typeof setupStore>;
export type AppDispatch = AppStoreType["dispatch"];
