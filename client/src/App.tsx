import "./App.scss";
import { useState } from "react";
import { useApiAuthPostRegisterMutation } from "./store/api/auth.api";

function App() {
  const [isAuth, setIsAuth] = useState(false);

  const [apiAuthPostRegister, { data }] = useApiAuthPostRegisterMutation();

  return (
    <>
      <div className="GigaHeader">
        <input type="text" placeholder="Логин" />
        <input type="password" placeholder="Пароль" />
        <button type="button">Войти</button>
        <button type="button">Зарегистрироваться</button>
        <p>
          Состояние авторизации: {isAuth ? "Авторизован" : "Не авторизован"}
        </p>
      </div>
      <div className="GigaChat">
        <div className="chatListWrapper">
          <ul className="chatList">
            {Array.from({ length: 30 }).map((_, index) => {
              return (
                <li>
                  <p>имя чата {index + 1}</p>
                </li>
              );
            })}
            <li>
              <p>+ Создать чат</p>
            </li>
          </ul>
        </div>
        <div className="chat">
          <div className="chatHeader">
            <p>имя чата</p>
            <p>количество пользователей</p>
          </div>
          <div className="chatBody">
            {Array.from({ length: 30 }).map((_, index) => {
              return (
                <div className={index % 2 === 0 ? "message notMe" : "message"}>
                  <p>Имя пользователя</p>
                  <p>Сообщение</p>
                </div>
              );
            })}
          </div>
          <div className="chatFooter"></div>
        </div>
      </div>
    </>
  );
}

export default App;
